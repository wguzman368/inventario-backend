package com.adsi.inventario.repository;

import com.adsi.inventario.model.Counter;
import org.springframework.data.jpa.repository.JpaRepository;

public interface CounterRepository  extends JpaRepository<Counter, Long> {

}
