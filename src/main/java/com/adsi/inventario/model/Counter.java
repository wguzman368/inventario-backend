package com.adsi.inventario.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import javax.persistence.*;
import javax.xml.crypto.Data;

@Entity
@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class Counter {
    @Id
    @GeneratedValue(strategy = GenerationType.AUTO)
    @Column(columnDefinition = "serial")
    private Long idUser;
    @Column(length = 50)
    private String equipment;
    @Column(length = 50)
    private  Long equipSerie;
    @Column(length = 50)
    private  String mark;
    @Column(length = 50)
    private String user;

    private Number buyDate;
    @Column(length = 50)
    private Number price;





}
