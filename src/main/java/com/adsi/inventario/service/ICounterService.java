package com.adsi.inventario.service;
import com.adsi.inventario.model.Counter;

import java.util.Optional;

public interface ICounterService {
    //read method
    public Iterable<Counter> read();
    //create method
    public Counter create ( Counter counter);
    //Update method
    public  Counter update( Counter counter);
    //delete method
    public void delete(Long id);
    //search method
    public Optional< Counter> getById(Long id);
}
