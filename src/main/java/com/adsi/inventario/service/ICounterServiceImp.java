package com.adsi.inventario.service;

import com.adsi.inventario.model.Counter;
import com.adsi.inventario.repository.CounterRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Optional;

@Service
public class ICounterServiceImp implements ICounterService {
   @Autowired
   CounterRepository counterRepository;
    @Override
    public Iterable<Counter> read() {
        return counterRepository.findAll();
    }

    @Override
    public Counter create(Counter counter) {
        return counterRepository.save(counter);
    }

    @Override
    public Counter update(Counter counter) {
        return counterRepository.save(counter);
    }

    @Override
    public void delete(Long id) {
       counterRepository.deleteById(id);

    }

    @Override
    public Optional<Counter> getById(Long id) {
        return counterRepository.findById(id);
    }
}
