package com.adsi.inventario.web.rest;

import com.adsi.inventario.model.Counter;
import com.adsi.inventario.service.ICounterService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.Optional;

@RestController
@RequestMapping("/api")
public class CounterResource {
       @Autowired
       ICounterService counterService;


       @GetMapping("/counter")
       public Iterable<Counter> read() {
           return counterService.read();
       }
       @PostMapping("/counter")
       public Counter create (@RequestBody Counter counter){
            return counterService.create(counter);
        }
       @PutMapping("/counter")
       public Counter update(@RequestBody Counter counter){
            return counterService.update(counter);
        }
       @DeleteMapping("/counter/{Id}")
       public void delete (@PathVariable Long id){
            counterService.delete(id);
        }

       @GetMapping("/counter/{Id}")
       public Optional<Counter> getById (@PathVariable Long id){
            return counterService.getById(id);

        }
    }









